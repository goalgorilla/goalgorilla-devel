<?php
/**
 * @file
 * gg_devel.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function gg_devel_user_default_roles() {
  $roles = array();

  // Exported role: devel.
  $roles['devel'] = array(
    'name' => 'devel',
    'weight' => '2',
  );

  return $roles;
}
